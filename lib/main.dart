import 'package:async/async.dart';
import 'package:flutter/material.dart';

import 'mainbloc.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Bloc'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();



}

class _MyHomePageState extends State<MyHomePage> {

  final MainBloc mb = MainBloc();




  CancelableOperation<void> cancellableOperation;

  void cancelar() async {
    if(!cancellableOperation.isCanceled){
      await cancellableOperation.cancel();
    }else{
      print("la tarea ya ha sido cancelada anteriormente");
    }
  }

  Future<void> tarea() async {
    await Future.delayed(const Duration(seconds : 5));
    print("tarea completada");
  }

  Future<void> ejecutar() async {

    cancellableOperation = CancelableOperation.fromFuture(
//      Future.delayed(const Duration(seconds : 5), (){ return print("completo..");}),
      tarea(),
      onCancel: () => { print("Usted ha cancelado la tarea") },
    );

//    await cancellableOperation.valueOrCancellation();
    return cancellableOperation.valueOrCancellation;
  }

  final CancelableCompleter<bool> _completer = CancelableCompleter(onCancel: () => false);

  Future<bool> _submit() async {
    _completer.complete(Future.value(_solve()));
    return _completer.operation.value;
  }

  // This is just a simple method that will finish the future in 5 seconds
  Future<bool> _solve() async {
    return await Future.delayed(Duration(seconds: 20), () { print("completado"); return true;});
  }

  void _cancel() async {
    var value = await _completer.operation.cancel();
    // if we stopped the future, we get false
    assert(value == false);
  }






  @override
  void dispose() {
    mb.dispose();
    super.dispose();

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            StreamBuilder(
              initialData: 0,
              stream: mb.stateStream,
              builder: (BuildContext b, AsyncSnapshot a){
                return Text(
                  'Mostrar dato: ${a.data}',
                );
              }
            ),
            RaisedButton(
              onPressed: (){
                mb.eventSink.add(Dismunuir());
              },
              child: Text("Disminuir"),
            ),
            RaisedButton(
              onPressed:  () async {  bool _isFutureCompleted = await _submit(); },
              child: Text("Ejecutar"),
            ),
            RaisedButton(
              onPressed: _cancel,
              child: Text("Cancelar"),
            )

          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          print("ok");
          mb.eventSink.add(Incrementar());
        },
        tooltip: 'flotante',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
