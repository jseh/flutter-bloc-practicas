import 'dart:async';

abstract class Event{}
class Incrementar extends Event{}
class Dismunuir extends Event{}
// sinks entradas, streams salidas
class MainBloc{
  // estado inicial
  int contador = 0;

  final StreamController<int> stateController = StreamController<int>();
  // es el stream a usar en la interfaz
  Stream<int> get stateStream => stateController.stream;
  StreamSink<int> get stateSink => stateController.sink;


  // en teoria deberia haber un manejador de eventos por bloc
  final StreamController<Event> eventController = StreamController<Event>();
  // sirve para recibir los eventos en el bloc(este archivo) y ejecutar acciones segun el tipo de evento
  Stream<Event> get eventStream => eventController.stream;
  // sirve para enviar eventos al bloc desde la interfaz
  StreamSink<Event> get eventSink => eventController.sink;



  MainBloc(){
    eventStream.listen(mapearEventos);
  }

  void mapearEventos(Event e){
    if(e is Incrementar){
      contador++;
    } else {
      if(e is Dismunuir){
        contador--;
      }
    }
    // hago los cambios al estado y despues los notifico
    stateController.add(contador);


  }


  void dispose(){
    eventController.close();
    stateController.close();
  }
}